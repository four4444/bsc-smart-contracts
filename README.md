# bsc-smart-contracts

FrozenToken.sol

Дополнительные функции контракта:

activePrivateRound() - активирует старт предпродаж токена

activeSales() - активирует старт основных продаж токена (деактивирует предпродажи)

stopSales() - останавливает продажу (выпуск) токенов

getSales() - активная продажа (1 - предпродажа, 2 - основная продажа)

setRoot(address _root) - устанавливает адрес имеющий право вызывать некоторые функции

mintOwner(address user, uint256 amount) - чеканка токенов (только root или owner)

mint(address user, uint256 amount) - чеканка токенов для предпродаж и основных продаж с функциями заморозки и стекирования

migrationMint(address user, uint256 amount, bool add) - миграция токенов пользователям сфункцией заморозки

------------------------------------------------------------------------------------------------------------------

BonusContract.sol

antiSabotage(bool _lowBalance) - предотвращение использования функции withdrawBonus, если на контракте недостаточно средств

withdrawBonus(uint256 amount, bytes calldata signatureDars,bytes calldata signatureCompany) - вывод накопленных бонусов

bMigrations(address _user, uint256 _totalBuy, uint256 _totalBuyOutside, uint256 _totalBuySpecial, uint256 _affectedBuySpecial, uint256 _totalUpgrade) - миграция пользователей

buyOutside(address user,uint256 price,uint256 marketing)
buy(uint256 price,bytes32 orderUID)
buySpecial(uint256 price,bytes32 singlePacketUID) - функции покупки пакетов

